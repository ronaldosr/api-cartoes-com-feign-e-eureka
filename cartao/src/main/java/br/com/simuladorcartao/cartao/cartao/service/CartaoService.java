package br.com.simuladorcartao.cartao.cartao.service;

import br.com.simuladorcartao.cartao.cartao.dataprovider.model.Cartao;
import br.com.simuladorcartao.cartao.cartao.dataprovider.repository.CartaoRepository;
import br.com.simuladorcartao.cartao.cartao.exception.ResourceNotFoundException;
import br.com.simuladorcartao.cartao.cartao.gateway.cliente.exception.ClienteNotFoundException;
import br.com.simuladorcartao.cartao.cartao.gateway.ClienteGateway;
import br.com.simuladorcartao.cartao.cartao.gateway.cliente.response.ClienteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleInfoNotFoundException;
import javax.xml.crypto.Data;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteGateway clienteGateway;

    /**
     *
     * @param clienteId
     * @param cartao
     * @return
     */
    public Cartao cadastrarCartao(long clienteId, Cartao cartao) {
        try {
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(clienteId);
            cartao.setIdCliente(clienteResponse.getId());
            cartao.setAtivo(true);
            return cartaoRepository.save(cartao);
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o cartão: " + e);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro no serviço de cartão: " + e);
        }
    }

    /**
     * Atualizar a situação do cartão por Id
     * @param id
     * @param situacao
     * @return
     */
    public Cartao atualizarSituacaoCartao(long id, boolean situacao) {
        try {
            Cartao cartao = consultarCartaoPorId(id);
            cartao.setAtivo(situacao);
            return cartaoRepository.save(cartao);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao atualizar a situação do cartão: " + e);
        }
    }

    /**
     * Consultar a situação do Cartão
     * @param id
     * @return
     */
    public Cartao consultarCartaoPorId(long id) {
        try {
            Optional<Cartao> cartao = cartaoRepository.findById(id);
            if (!cartao.isPresent()) {
                throw new ResourceNotFoundException("Cartão não encontrado com id " + id);
            }
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(cartao.get().getIdCliente());
            return cartao.get();
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException(e);
        }
    }
}
