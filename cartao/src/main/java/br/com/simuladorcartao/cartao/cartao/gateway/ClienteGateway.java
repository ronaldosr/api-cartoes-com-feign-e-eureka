package br.com.simuladorcartao.cartao.cartao.gateway;

import br.com.simuladorcartao.cartao.cartao.gateway.config.GatewayConfiguration;
import br.com.simuladorcartao.cartao.cartao.gateway.cliente.response.ClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = GatewayConfiguration.class)
public interface ClienteGateway {

    @GetMapping("/cliente/{id}")
    ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id);
}
