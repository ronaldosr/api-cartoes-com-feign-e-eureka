package br.com.simuladorcartao.cartao.cartao.dataprovider.repository;

import br.com.simuladorcartao.cartao.cartao.dataprovider.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository extends JpaRepository<Cartao, Long> {
}
