package br.com.simuladorcartao.cartao.cartao.gateway.cliente.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente não encontrado")
public class ClienteNotFoundException extends RuntimeException {

    public ClienteNotFoundException(Throwable cause) {
        super(cause);
    }

    public ClienteNotFoundException() {
    }
}
