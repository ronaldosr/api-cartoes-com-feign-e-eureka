package br.com.simuladorcartao.cartao.cartao.controller;

import br.com.simuladorcartao.cartao.cartao.controller.request.CadastroCartaoRequest;
import br.com.simuladorcartao.cartao.cartao.controller.request.SituacaoCartaoRequest;
import br.com.simuladorcartao.cartao.cartao.controller.response.CartaoResponse;
import br.com.simuladorcartao.cartao.cartao.dataprovider.model.Cartao;
import br.com.simuladorcartao.cartao.cartao.mapper.CartaoMapper;
import br.com.simuladorcartao.cartao.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    /**
     * Cadastrar Cartao
     * @param cadastroCartaoRequest
     * @return cartaoResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse cadastrarCartao (@Valid @RequestBody CadastroCartaoRequest cadastroCartaoRequest) {
        Cartao cartao = cartaoService.cadastrarCartao(
                cadastroCartaoRequest.getClienteId(),
                cartaoMapper.converterParaCartao(cadastroCartaoRequest));
        return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
    }

    /**
     * Atualizar Situação do Cartão
     * @param id
     * @param situacaoCartaoRequest
     * @return
     */
    @PatchMapping("/{id}")
    public CartaoResponse atualizarSituacaoCartao (@PathVariable(name = "id") long id,
                                                   @Valid @RequestBody SituacaoCartaoRequest situacaoCartaoRequest) {
        Cartao cartao = cartaoService.atualizarSituacaoCartao(id, situacaoCartaoRequest.isAtivo());
        return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
    }

    /**
     * Consultar Cartão por Id
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public CartaoResponse consultarCartaoPorId(@PathVariable(name = "id") long id) {
        Cartao cartao = cartaoService.consultarCartaoPorId(id);
        return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
    }

    /**
     * Bloquear cartão por Id
     * @param cartaoId
     */
    @PatchMapping("/{id}/expirar")
    public void bloquearCartao(@PathVariable(name = "cartaoId") long cartaoId) {
        cartaoService.atualizarSituacaoCartao(cartaoId, false);
    }
}
