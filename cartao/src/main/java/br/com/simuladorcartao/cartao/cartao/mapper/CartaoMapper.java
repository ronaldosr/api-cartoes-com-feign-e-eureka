package br.com.simuladorcartao.cartao.cartao.mapper;

import br.com.simuladorcartao.cartao.cartao.controller.request.CadastroCartaoRequest;
import br.com.simuladorcartao.cartao.cartao.controller.response.CartaoResponse;
import br.com.simuladorcartao.cartao.cartao.dataprovider.model.Cartao;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;

@Component
public class CartaoMapper {

    public Cartao converterParaCartao(CadastroCartaoRequest cadastroCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cadastroCartaoRequest.getNumero());
        return cartao;
    }

    public CartaoResponse converterParaCadastroCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getIdCliente());
        cartaoResponse.setAtivo(cartao.isAtivo());
        return cartaoResponse;
    }
}
