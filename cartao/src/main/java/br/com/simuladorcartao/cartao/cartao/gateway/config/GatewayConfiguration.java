package br.com.simuladorcartao.cartao.cartao.gateway.config;

import br.com.simuladorcartao.cartao.cartao.gateway.cliente.exception.ClienteDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class GatewayConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteDecoder();
    }
}
