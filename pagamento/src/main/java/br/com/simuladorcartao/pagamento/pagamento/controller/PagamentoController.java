package br.com.simuladorcartao.pagamento.pagamento.controller;

import br.com.simuladorcartao.pagamento.pagamento.controller.request.CadastroPagamentoRequest;
import br.com.simuladorcartao.pagamento.pagamento.controller.response.PagamentoResponse;
import br.com.simuladorcartao.pagamento.pagamento.dataprovider.model.Pagamento;
import br.com.simuladorcartao.pagamento.pagamento.mapper.PagamentoMapper;
import br.com.simuladorcartao.pagamento.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    /**
     * Cadastrar Pagamento
     * @param cadastroPagamentoRequest
     * @return cartaoResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse cadastrarPagamento(@RequestBody @Valid CadastroPagamentoRequest cadastroPagamentoRequest) {
        Pagamento pagamento = pagamentoService.cadastrarPagamento(cadastroPagamentoRequest.getCartao_id(),
                pagamentoMapper.converterParaPagamento(cadastroPagamentoRequest));
        return pagamentoMapper.converterParaPagamentoResponse(pagamento);
    }

    /**
     * Listar pagamento por Id Cartão
     * @param id_cartao
     * @return
     */
    @GetMapping("/{id_cartao}")
    public List<PagamentoResponse> consultarPagamentosPorId(@PathVariable(name = "id_cartao") long id_cartao) {
        List<Pagamento> listaPagamento = pagamentoService.consultarPagamentosPorId(id_cartao);
        return pagamentoMapper.convertarParaListaPagamentoResponse(listaPagamento);
    }

    /**
     * Efetuar pagamento
     * @param id_cartao
     */
    @PostMapping("/{id_cartao}/pagar")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void efetuarPagamento(@PathVariable(name = "id_cartao") long id_cartao ) {
        pagamentoService.efetuarPagamento(id_cartao);
    }

}
