package br.com.simuladorcartao.pagamento.pagamento.exception;

import com.netflix.ribbon.proxy.annotation.Http;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CartaoInativoException extends RuntimeException {
    public CartaoInativoException(String message) {
        super(message);
    }
}
