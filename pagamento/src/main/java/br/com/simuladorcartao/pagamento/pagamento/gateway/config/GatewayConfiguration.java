package br.com.simuladorcartao.pagamento.pagamento.gateway.config;

import br.com.simuladorcartao.pagamento.pagamento.gateway.cartao.exception.CartaoDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class GatewayConfiguration {

    @Bean
    public ErrorDecoder obterCartaoDecoder() {
        return new CartaoDecoder();
    }
}
