package br.com.simuladorcartao.pagamento.pagamento.gateway;

import br.com.simuladorcartao.pagamento.pagamento.gateway.cartao.response.CartaoResponse;
import br.com.simuladorcartao.pagamento.pagamento.gateway.config.GatewayConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = GatewayConfiguration.class)
public interface CartaoGateway {

    @GetMapping("/cartao/{id}")
    CartaoResponse consultarCartaoPorId(@PathVariable(name = "id") long id);
}
