package br.com.simuladorcartao.cliente.cliente.service;

import br.com.simuladorcartao.cliente.cliente.dataprovider.model.Cliente;
import br.com.simuladorcartao.cliente.cliente.dataprovider.repository.ClienteRepository;
import br.com.simuladorcartao.cliente.cliente.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    /**
     * Cadsatrar Cliente
     * @param cliente
     * @return cliente
     */
    public Cliente cadastrarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch(DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o cliente: " + e);
        } catch(RuntimeException e ) {
            throw new RuntimeException("Erro no serviço de cliente: " + e);
        }
    }

    /**
     * Consultar Cliente por Id
     * @param id
     * @return cliente
     */
    public Cliente consultarClientePorId(long id) {
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (!cliente.isPresent()) {
                throw new ResourceNotFoundException("Cliente não encontrado com id " + id);
            }
            return cliente.get();
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar o cliente com id " + id, e);
        }
    }
}
