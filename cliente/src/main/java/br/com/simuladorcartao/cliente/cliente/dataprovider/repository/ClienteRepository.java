package br.com.simuladorcartao.cliente.cliente.dataprovider.repository;

import br.com.simuladorcartao.cliente.cliente.dataprovider.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
