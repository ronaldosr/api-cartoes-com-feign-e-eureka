package br.com.simuladorcartao.cliente.cliente.mapper;

import br.com.simuladorcartao.cliente.cliente.controller.request.CadastroClienteRequest;
import br.com.simuladorcartao.cliente.cliente.controller.response.ClienteResponse;
import br.com.simuladorcartao.cliente.cliente.dataprovider.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    /**
     * Converter um Cliente para um Cliente Response
     * @param cliente
     * @return
     */
    public ClienteResponse converterParaCadastroClienteResponse(Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());
        return clienteResponse;
    }

    /**
     * Converte um cadastroClienteRequest em um Cliente
     * @param cadastroClienteRequest
     * @return
     */
    public Cliente converterParaCliente(CadastroClienteRequest cadastroClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(cadastroClienteRequest.getNome());
        return cliente;
    }
}
