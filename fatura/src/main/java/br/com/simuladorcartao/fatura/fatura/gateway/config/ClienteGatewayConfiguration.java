package br.com.simuladorcartao.fatura.fatura.gateway.config;

import br.com.simuladorcartao.fatura.fatura.gateway.cliente.exception.ClienteDecoder;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.fallback.ClienteFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteGatewayConfiguration {

    @Bean
    public ErrorDecoder obterClienteDecoder() {
        return new ClienteDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteFallback(), RetryableException.class)
                //.withCircuitBreaker()
                //.withBulkhead()
                .build();
        return Resilience4jFeign.builder(decorators);
    }
}
