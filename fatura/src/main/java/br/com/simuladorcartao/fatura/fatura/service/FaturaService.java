package br.com.simuladorcartao.fatura.fatura.service;


import br.com.simuladorcartao.fatura.fatura.dataprovider.model.Fatura;
import br.com.simuladorcartao.fatura.fatura.dataprovider.repository.FaturaRepository;
import br.com.simuladorcartao.fatura.fatura.gateway.CartaoGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.ClienteGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.PagamentoGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.exception.CartaoNotFoundException;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.request.SituacaoCartaoRequest;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.response.CartaoResponse;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.exception.ClienteNotFoundException;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.response.ClienteResponse;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.exception.PagamentoNotFoundException;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.response.PagamentoResponse;
import br.com.simuladorcartao.fatura.fatura.mapper.FaturaMapper;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private CartaoGateway cartaoGateway;

    @Autowired
    private ClienteGateway clienteGateway;

    @Autowired
    private PagamentoGateway pagamentoGateway;

    @Autowired
    private FaturaMapper faturaMapper;

    /**
     * Listar Faturas Pagas por Id Cliente
     * @param clienteId
     * @return
     */
    public List<Fatura> listarFaturasPagas(long clienteId, long cartaoId) {
        try {
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(clienteId);
            CartaoResponse cartaoResponse = cartaoGateway.consultarCartaoPorId(cartaoId);
            if (clienteResponse.getId() == cartaoResponse.getClienteId()) {
                List<Fatura> listaFatura = faturaRepository.findAllByIdCartaoAndIdCliente(cartaoId, clienteId);
                return listaFatura;
            } else {
                throw new RuntimeException("Cartão não pertence ao cliente");
            }
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException();
        }  catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException();
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar faturas pagas : " +
                    e.getMessage(), e);
        }
    }

    /**
     * Consultar Fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return listaPagamentos
     */
    public List<PagamentoResponse> consultarFatura(long clienteId, long cartaoId) {
        try {
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(clienteId);
            CartaoResponse cartaoResponse = cartaoGateway.consultarCartaoPorId(cartaoId);
            if (clienteResponse.getId() == cartaoResponse.getClienteId()) {
                return pagamentoGateway.consultarPagamentosPorId(cartaoId);
            } else {
                throw new RuntimeException("Cartão não pertence ao cliente");
            }
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException();
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException();
        } catch (PagamentoNotFoundException e) {
          throw new PagamentoNotFoundException();
        } catch (FeignException e) {
            throw new RuntimeException("Erro na requisição remota: " +
                    e.getMessage(), e);
        } catch (RuntimeException e ) {
            throw new RuntimeException("Erro ao consultar fatura: " +
                    e.getMessage(), e);
        }
    }

    /**
     * Pagar Fatura utilizando Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @Transactional
    public Fatura pagarFatura(long clienteId, long cartaoId) {
        try {
            List<PagamentoResponse> listaPagamentoResponse = consultarFatura(clienteId, cartaoId);
            Fatura fatura = criarFatura(listaPagamentoResponse, clienteId, cartaoId);
            if (fatura.getValorPago().compareTo(BigDecimal.ZERO) > 0) {
               pagamentoGateway.efetuarPagamento(cartaoId);
                return faturaRepository.save(fatura);
            } else {
                throw new RuntimeException("Não há lançamentos para faturar.");
            }
        } catch (ClienteNotFoundException e) {
            throw new ClienteNotFoundException();
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException();
        } catch (PagamentoNotFoundException e) {
            throw new PagamentoNotFoundException();
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao pagar fatura: " +
                    e.getCause().getCause().getMessage(), e);
        } catch (FeignException e) {
            throw new RuntimeException("Erro na requisição remota: " +
                    e.getMessage(), e);
        }

    }

    /**
     * Bloquear Cartão com base no Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     */
    @Transactional
    public void bloquearCartao(long clienteId, long cartaoId) {
        try {
            ClienteResponse clienteResponse = clienteGateway.consultarClientePorId(clienteId);
            CartaoResponse cartaoResponse = cartaoGateway.consultarCartaoPorId(cartaoId);
            SituacaoCartaoRequest situacaoCartaoRequest = new SituacaoCartaoRequest();
            situacaoCartaoRequest.setAtivo(false);
            cartaoGateway.atualizarSituacaoCartao(cartaoId, situacaoCartaoRequest);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao bloquear cartão: " +
                    e.getCause().getCause().getMessage(), e);
        } catch (FeignException e) {
            throw new RuntimeException("Erro na requisição remota: " +
                    e.getMessage(), e);
        }
    }

    /**
     * Criar Fatura
     * @param listaPagamentoResponse
     * @param clienteId
     * @param cartaoId
     * @return fatura
     */
    @Transactional
    private Fatura criarFatura(List<PagamentoResponse> listaPagamentoResponse, long clienteId, long cartaoId) {
        try {
            CartaoResponse cartaoResponse = cartaoGateway.consultarCartaoPorId(cartaoId);

            Fatura fatura = new Fatura();
            fatura.setIdCliente(cartaoResponse.getClienteId());
            fatura.setIdCartao(cartaoResponse.getId());
            fatura.setValorPago(calcularValorFatura(listaPagamentoResponse));
            fatura.setPagoEm(LocalDate.now());
            return fatura;
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException();
        } catch (FeignException e) {
            throw new RuntimeException("Erro na requisição remota: " +
                    e.getMessage(), e);
        }
    }

    /**
     * Calcula o valor da fatura sumarizazndo os pagamentos do cliente
     * @param listaPagamentoResponse
     * @return BigDecimal
     */
    private BigDecimal calcularValorFatura(List<PagamentoResponse> listaPagamentoResponse) {
        BigDecimal valorFatura = new BigDecimal(0.00);
        for (PagamentoResponse pagamentoResponse : listaPagamentoResponse) {
            valorFatura = valorFatura.add(pagamentoResponse.getValor());
        }
        return valorFatura;
    }
}
