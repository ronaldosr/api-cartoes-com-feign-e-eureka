package br.com.simuladorcartao.fatura.fatura.mapper;

import br.com.simuladorcartao.fatura.fatura.controller.response.FaturaPagaResponse;
import br.com.simuladorcartao.fatura.fatura.controller.response.FaturaResponse;
import br.com.simuladorcartao.fatura.fatura.dataprovider.model.Fatura;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.response.PagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FaturaMapper {

    public List<FaturaPagaResponse> converterParaListaPagamentoResponse(List<PagamentoResponse> listaPagamentoResponse) {
        List<FaturaPagaResponse> listaFaturaPagaResponse = new ArrayList<>();
        for (PagamentoResponse pagamentoResponse : listaPagamentoResponse) {
            FaturaPagaResponse faturaPagaResponse = new FaturaPagaResponse();
            faturaPagaResponse.setId(pagamentoResponse.getId());
            faturaPagaResponse.setCartao_id(pagamentoResponse.getCartao_id());
            faturaPagaResponse.setDescricao(pagamentoResponse.getDescricao());
            faturaPagaResponse.setValor(pagamentoResponse.getValor());
            listaFaturaPagaResponse.add(faturaPagaResponse);
        }
        return listaFaturaPagaResponse;
    }

    public FaturaResponse converterParaComprovanteFaturaResponse(Fatura fatura) {
        FaturaResponse faturaResponse = new FaturaResponse();
        faturaResponse.setId(fatura.getId());
        faturaResponse.setValorPago(fatura.getValorPago());
        faturaResponse.setPagoEm(fatura.getPagoEm());
        return faturaResponse;
    }

    public List<FaturaResponse> converterParaListaFaturaResponse(List<Fatura> listaFatura) {
        List<FaturaResponse> listaFaturaResponse = new ArrayList<>();
        for (Fatura fatura : listaFatura) {
            FaturaResponse faturaResponse = new FaturaResponse();
            faturaResponse.setId(fatura.getId());
            faturaResponse.setPagoEm(fatura.getPagoEm());
            faturaResponse.setValorPago(fatura.getValorPago());
            listaFaturaResponse.add(faturaResponse);
        }
        return listaFaturaResponse;
    }
}
