package br.com.simuladorcartao.fatura.fatura.gateway.config;

import br.com.simuladorcartao.fatura.fatura.gateway.cartao.exception.CartaoDecoder;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.fallback.CartaoFallback;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.fallback.ClienteFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoGatewayConfiguration {

    @Bean
    public ErrorDecoder obterCartaoDecoder() {
        return new CartaoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoFallback(), RetryableException.class).build();
        return Resilience4jFeign.builder(decorators);
    }
}
