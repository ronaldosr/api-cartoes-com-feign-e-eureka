package br.com.simuladorcartao.fatura.fatura.gateway.cartao.fallback;

import br.com.simuladorcartao.fatura.fatura.gateway.CartaoGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.request.SituacaoCartaoRequest;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.response.CartaoResponse;

import javax.validation.Valid;

public class CartaoFallback implements CartaoGateway {

    @Override
    public CartaoResponse consultarCartaoPorId(long id) {
        return null;
    }

    @Override
    public CartaoResponse atualizarSituacaoCartao(long id, @Valid SituacaoCartaoRequest situacaoCartaoRequest) {
        return null;
    }
}
