package br.com.simuladorcartao.fatura.fatura.gateway.pagamento.fallback;

import br.com.simuladorcartao.fatura.fatura.gateway.PagamentoGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.response.PagamentoResponse;

import java.util.List;

public class PagamentoFallback implements PagamentoGateway {
    @Override
    public List<PagamentoResponse> consultarPagamentosPorId(long id_cartao) {
        return null;
    }

    @Override
    public void efetuarPagamento(long id_cartao) {

    }
}
