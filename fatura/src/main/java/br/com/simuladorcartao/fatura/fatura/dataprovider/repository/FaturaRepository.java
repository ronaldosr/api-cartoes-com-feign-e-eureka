package br.com.simuladorcartao.fatura.fatura.dataprovider.repository;

import br.com.simuladorcartao.fatura.fatura.dataprovider.model.Fatura;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FaturaRepository extends JpaRepository<Fatura, Long> {
    List<Fatura> findAllByIdCartaoAndIdCliente(long cartaoId, long clienteId);
}
