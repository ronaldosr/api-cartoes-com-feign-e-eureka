package br.com.simuladorcartao.fatura.fatura.controller;

import br.com.simuladorcartao.fatura.fatura.controller.response.BloqueioCartaoResponse;
import br.com.simuladorcartao.fatura.fatura.controller.response.FaturaPagaResponse;
import br.com.simuladorcartao.fatura.fatura.controller.response.FaturaResponse;
import br.com.simuladorcartao.fatura.fatura.dataprovider.model.Fatura;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.response.PagamentoResponse;
import br.com.simuladorcartao.fatura.fatura.mapper.FaturaMapper;
import br.com.simuladorcartao.fatura.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @Autowired
    private FaturaMapper faturaMapper;

    /**
     * Consultar a fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<FaturaPagaResponse> consultarFatura(@PathVariable(name = "cliente-id") long clienteId,
                                                    @PathVariable(name = "cartao-id") long cartaoId) {
        List<PagamentoResponse> listaPagamentoResponse = faturaService.consultarFatura(clienteId, cartaoId);
        return faturaMapper.converterParaListaPagamentoResponse(listaPagamentoResponse);
    }

    /**
     * Pagar fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public FaturaResponse pagarFatura(@PathVariable(name="cliente-id") long clienteId,
                                      @PathVariable(name="cartao-id") long cartaoId) {
        Fatura fatura = faturaService.pagarFatura(clienteId, cartaoId);
        return faturaMapper.converterParaComprovanteFaturaResponse(fatura);
    }

    /**
     * Bloquear o Cartão por Id do Clieeente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public BloqueioCartaoResponse bloquearCartao(@PathVariable(name="cliente-id") long clienteId,
                                                 @PathVariable(name="cartao-id") long cartaoId) {
        BloqueioCartaoResponse bloqueioCartaoResponse = new BloqueioCartaoResponse();
        faturaService.bloquearCartao(clienteId, cartaoId);
        bloqueioCartaoResponse.setStatus("ok");;
        return bloqueioCartaoResponse;
    }

    @GetMapping("/{cliente-id}/{cartao-id}/pagas")
    public List<FaturaResponse> listarFaturasPagas(@PathVariable(name = "cliente-id") long clienteId,
                                                       @PathVariable(name = "cartao-id") long cartaoId) {
        List<Fatura> listaFatura = faturaService.listarFaturasPagas(clienteId, cartaoId);
            return faturaMapper.converterParaListaFaturaResponse(listaFatura);
    }

}
