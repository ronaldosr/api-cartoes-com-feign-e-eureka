package br.com.simuladorcartao.fatura.fatura.gateway.config;

import br.com.simuladorcartao.fatura.fatura.gateway.cartao.exception.CartaoDecoder;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.fallback.CartaoFallback;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.exception.ClienteDecoder;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.exception.PagamentoDecoder;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.fallback.PagamentoFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

public class PagamentoGatewayConfiguration {

    @Bean
    public ErrorDecoder obterPagamentoDecoder() {
        return new PagamentoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new PagamentoFallback(), RetryableException.class).build();
        return Resilience4jFeign.builder(decorators);
    }
}
