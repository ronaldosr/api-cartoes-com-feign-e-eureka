package br.com.simuladorcartao.fatura.fatura.gateway;

import br.com.simuladorcartao.fatura.fatura.gateway.config.PagamentoGatewayConfiguration;
import br.com.simuladorcartao.fatura.fatura.gateway.pagamento.response.PagamentoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "PAGAMENTO", configuration = PagamentoGatewayConfiguration.class)
public interface PagamentoGateway {

    @GetMapping("/pagamentos/{id_cartao}")
    List<PagamentoResponse> consultarPagamentosPorId(@PathVariable(name = "id_cartao") long id_cartao);

    @PostMapping("/pagamentos/{id_cartao}/pagar")
    void efetuarPagamento(@PathVariable(name = "id_cartao") long id_cartao);
}
